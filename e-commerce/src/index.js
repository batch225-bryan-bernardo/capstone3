import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

// Importing the CSS Bootstrap
import 'bootstrap/dist/css/bootstrap.min.css';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <>

    <App />

  </>
);

