import './App.css';
import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import Products from './pages/Products';
import AdminProducts from './pages/AdminProducts';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Register from './pages/Register';
import Cart from './pages/Cart';
import BuyNowCheckout from './pages/BuyNowCheckout';
import CartCheckout from './pages/CartCheckout';
import ProductView from './components/ProductView';
import AdminProductView from './components/AdminProductView';
import ProductDetails from './components/ProductDetails';
import { UserProvider } from './UserContext'
import AdminDashboard from './pages/AdminDashboard';
import AdminNavbar from './components/AdminNavbar';
import { useState, useEffect } from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'
import CreateProduct from './pages/CreateProduct';
import UpdateProduct from './pages/UpdateProduct';
import UserOrders from './pages/UserOrder';
import AdminOrders from './pages/AdminOrders';
import AdminAllUsers from './pages/AdminAllUsers';
import Header from './components/Header';
import Footer from './components/Footer';
import ErrorPage from './pages/ErrorPage';

function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })

  const unsetUser = () => {
    localStorage.clear()
  }

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/getUser/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
      .then(res => res.json())
      .then(data => {

        // Set the user states values with the user details upon successful login.
        if (typeof data._id !== "undefined") {

          setUser({
            id: data._id,
            isAdmin: data.isAdmin
          });

          // Else set the user states to the initial values
        } else {
          setUser({
            id: null,
            isAdmin: null
          });
        }
      })
  }, []);
  console.log(user)

  const userLoggedin = (user.id !== null)
  const userIsAdmin = (user.isAdmin)
  return (
    <UserProvider value={{ user, setUser, unsetUser }}>
      <Router>
        <Header />
        {(userLoggedin && userIsAdmin) ?
          <AdminNavbar />
          :
          <AppNavbar />
        }
        <Routes>
          <Route path='/' element={<Home />} />
          <Route path='/products' element={<Products />} />
          <Route path="/products/:productId" element={<ProductView />} />
          <Route path="/login" element={<Login />} />
          <Route path="/register" element={<Register />} />
          <Route path="/logout" element={<Logout />} />
          <Route path="/cart" element={<Cart />} />
          <Route path="/cartCheckout" element={<CartCheckout />} />
          <Route path="/checkout/:productId" element={<BuyNowCheckout />} />
          <Route path="/userOrders" element={<UserOrders />} />
          <Route path="*" element={<ErrorPage />} />

          {(userLoggedin && userIsAdmin) ?
            <>
              <Route path='/allProducts' element={<AdminProducts />} />
              <Route path='/allProducts/:productId' element={<AdminProductView />} />
              <Route path="/admin" element={<AdminDashboard />} />
              <Route path="/createProducts" element={<CreateProduct />} />
              <Route path="/updateProducts/:productId" element={<UpdateProduct />} />
              <Route path="/orders" element={<AdminOrders />} />
              <Route path="/allUsers" element={<AdminAllUsers />} />
            </>
            :
            null
          }
        </Routes>
        <Footer />
      </Router>
    </UserProvider>
  );
}


export default App;
