import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col, CardImg, Stack, Form } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'


export default function Update() {

    // Gets the courseId from the URL of the route that this component is connected to. '/courses/:courseId'
    const { productId } = useParams()
    const { user } = useContext(UserContext)
    const token = localStorage.getItem('token')
    const [name, setName] = useState("");
    const [description, setDescription] = useState("");
    const [price, setPrice] = useState(0);
    const [stocks, setStocks] = useState(0);
    const [image, setImage] = useState("");
    const [status, setStatus] = useState('')
    const navigate = useNavigate()

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/products/singleProduct/${productId}`, {
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${token}`
            }
        })
            .then(response => {
                if (!response.ok) {
                    throw new Error('Failed to fetch product');
                }
                return response.json();
            })
            .then(result => {
                setName(result.name)
                setDescription(result.description)
                setPrice(result.price)
                setStatus(result.isActive)
                setStocks(result.stocks)
                setImage(result.image)
            })
            .catch(error => {
                console.error(error);
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Failed to fetch product!'
                });
            });
    }, [productId])

    const handleSubmit = (event) => {
        event.preventDefault();
        const token = localStorage.getItem('token')
        if (user.isAdmin) {
            fetch(`${process.env.REACT_APP_API_URL}/products/productUpdate/${productId}`, {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`
                },
                body: JSON.stringify({
                    "name": name,
                    "description": description,
                    "price": price,
                    "stocks": stocks,
                    "image": image
                })
            })
                .then(response => response.json())
                .then(result => {
                    if (result) {
                        console.log("Received result:", result);
                        Swal.fire({
                            title: "Success!",
                            icon: "success",
                            text: "Product Updated Successfully!"
                        });
                        navigate("/allProducts")
                    } else {
                        console.log(result);

                        Swal.fire({
                            title: "Something went wrong!",
                            icon: "error",
                            text: "Please try again"
                        });
                    }
                }).catch(error => {
                    console.error(error);
                    Swal.fire({
                        title: "Something went wrong!",
                        icon: "error",
                        text: "Please try again"
                    });
                });
        }



    }

    console.log(status)
    return (
        <div className="container  mb-4">
            <h1>Update Product</h1>
            <Form onSubmit={handleSubmit}>
                <Form.Group controlId="name">
                    <Form.Label>Name</Form.Label>
                    <Form.Control
                        type="text"
                        value={name}
                        onChange={(e) => setName(e.target.value)}
                        required
                    />
                </Form.Group>
                <Form.Group controlId="description">
                    <Form.Label>Description</Form.Label>
                    <Form.Control
                        as="textarea"
                        rows={3}
                        value={description}
                        onChange={(e) => setDescription(e.target.value)}
                        required
                    />
                </Form.Group>
                <Form.Group controlId="price">
                    <Form.Label>Price</Form.Label>
                    <Form.Control
                        type="text"
                        pattern="[0-9]*"
                        inputmode="numeric"
                        placeholder="Enter price"
                        value={price}
                        onChange={(e) => setPrice(e.target.value)}
                        required
                    />
                </Form.Group>
                <Form.Group controlId="price">
                    <Form.Label>Stocks:</Form.Label>
                    <Form.Control
                        type="text"
                        pattern="[0-9]*"
                        inputmode="numeric"
                        placeholder="Enter stocks"
                        value={stocks}
                        onChange={(e) => setStocks(e.target.value)}
                        required
                    />
                </Form.Group>
                <Form.Group controlId="image">
                    <Form.Label>Image</Form.Label>
                    <Form.Group controlId="image">
                        <Form.Label>Image URL</Form.Label>
                        <Form.Control type="text" onBlur={(e) => setImage(e.target.value)} />
                        {<img src={image} alt="Product" />}
                    </Form.Group>
                </Form.Group>
                {(name == "" || description == "" || price == 0 || price == "" || stocks === "") ?
                    <div className='d-flex justify-content-end'>
                        <Button type="submit" variant="primary" onClick={handleSubmit} disabled >
                            Update Product
                        </Button>
                    </div>
                    :
                    <div className='d-flex justify-content-end'>
                        <Button type="submit" variant="primary" onClick={handleSubmit}>
                            Update Product
                        </Button>
                    </div>
                }
            </Form>
        </div>
    );
}