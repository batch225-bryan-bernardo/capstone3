import { useState, useEffect, useContext } from 'react'
import { Form, Button, Container, Nav } from 'react-bootstrap'
import { useNavigate, Navigate } from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'
import '../App.css'

function Login() {
    // Initializes the use of the properties from the UserProvider in App.js file
    const { user, setUser } = useContext(UserContext)

    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')

    // Initialize useNavigate
    // const navigate = useNavigate()

    // For determining if button is disabled or not
    const [isActive, setIsActive] = useState(false)

    const retrieveUser = (userId, token) => {
        fetch(`${process.env.REACT_APP_API_URL}/users/getUser/details`, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
            .then(response => response.json())
            .then(result => {
                // Store the user details retrieved from the token into the global user state
                setUser({
                    id: result._id,
                    isAdmin: result.isAdmin
                })
                console.log(result)
            })

    }

    function authenticate(event) {
        event.preventDefault()
        fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })

        })
            .then(response => response.json())
            .then(result => {
                if (typeof result.access !== "undefined") {

                    const token = result.access

                    localStorage.setItem('token', token)

                    retrieveUser(user.id, token)

                    console.log(token)
                    Swal.fire({
                        title: 'Login Successful!',
                        icon: 'success',
                        text: 'Welcome!'
                    })
                } else {
                    Swal.fire({
                        title: 'Login Failed!',
                        icon: 'error',
                        text: 'Invalid Email or password'
                    })
                }
            }).catch(error => {
                Swal.fire({
                    title: 'Error!',
                    icon: 'error',
                    text: 'An error has occured. Please try again!'
                })
            })
    }

    useEffect(() => {
        if ((email !== '' && password !== '')) {
            // Enables the submit button if the form data has been verified
            setIsActive(true)
        } else {
            setIsActive(false)
        }
    }, [email, password])
    if (user.id !== null) {
        if (user.isAdmin === true) {
            return <Navigate to="/admin" />;
        } else {
            return <Navigate to="/products" />;
        }
    }

    return (

        <Container className="log-container main-content align-items-center">
            <div>
                <Form onSubmit={event => authenticate(event)} >
                    <Form.Group controlId="userEmail" className="mt-4">
                        <Form.Label>Email address</Form.Label>
                        <Form.Control
                            type="email"
                            placeholder="Enter email"
                            value={email}
                            onChange={event => setEmail(event.target.value)}
                            required
                        />
                    </Form.Group>

                    <Form.Group controlId="password" className="mt-4">
                        <Form.Label>Password</Form.Label>
                        <Form.Control
                            type="password"
                            placeholder="Enter Password"
                            value={password}
                            onChange={event => setPassword(event.target.value)}
                            className="mb-4"
                            required
                        />
                    </Form.Group>

                    {isActive ?

                        <Button variant="primary" type="submit" id="submitBtn">
                            Submit
                        </Button>
                        :
                        <Button variant="primary" type="submit" id="submitBtn" disabled>
                            Submit
                        </Button>
                    }

                </Form>
            </div>
        </Container >
    )
}


export default Login;