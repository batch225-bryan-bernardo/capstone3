import React, { useContext, useState } from "react";
import UserContext from "../UserContext";
import { useNavigate } from "react-router-dom";
import Swal from 'sweetalert2'
import { Form, Button } from 'react-bootstrap';


function CreateProduct() {
    const [name, setName] = useState("");
    const [description, setDescription] = useState("");
    const [price, setPrice] = useState("");
    const [stocks, setStocks] = useState("");
    const { user } = useContext(UserContext);
    const navigate = useNavigate();
    const [image, setImage] = useState(null);
    const [imageUrl, setImageUrl] = useState(null);

    console.log(user.isAdmin)

    const handleSubmit = (event) => {
        event.preventDefault();
        const token = localStorage.getItem('token')
        if (user.isAdmin) {
            fetch(`${process.env.REACT_APP_API_URL}/products/createProduct`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`
                },
                body: JSON.stringify({
                    "name": name,
                    "description": description,
                    "price": price,
                    "stocks": stocks,
                    "image": image
                })
            })
                .then(response => response.json())
                .then(result => {
                    if (result) {
                        console.log("Received result:", result);
                        Swal.fire({
                            title: "Success!",
                            icon: "success",
                            text: "Product Successfully Created!"
                        });

                        navigate('/admin');
                    } else {
                        console.log(result);

                        Swal.fire({
                            title: "Something went wrong!",
                            icon: "error",
                            text: "Please try again"
                        });
                    }
                }).catch(error => {
                    console.error(error);
                    Swal.fire({
                        title: "Something went wrong!",
                        icon: "error",
                        text: "Please try again"
                    });
                });
        }
    };


    return (
        <div className="container  mb-4">
            <h1>Create Product</h1>
            <Form onSubmit={handleSubmit}>
                <Form.Group controlId="name">
                    <Form.Label>Name</Form.Label>
                    <Form.Control
                        type="text"
                        value={name}
                        onChange={(e) => setName(e.target.value)}
                        required
                    />
                </Form.Group>
                <Form.Group controlId="description">
                    <Form.Label>Description</Form.Label>
                    <Form.Control
                        as="textarea"
                        rows={3}
                        value={description}
                        onChange={(e) => setDescription(e.target.value)}
                        required
                    />
                </Form.Group>
                <Form.Group controlId="price">
                    <Form.Label>Price</Form.Label>
                    <Form.Control
                        type="text"
                        pattern="[0-9]*"
                        inputmode="numeric"
                        placeholder="Enter price"
                        value={price}
                        onChange={(e) => setPrice(e.target.value)}
                        required
                    />
                </Form.Group>
                <Form.Group controlId="stocks">
                    <Form.Label>Stocks:</Form.Label>
                    <Form.Control
                        type="text"
                        pattern="[0-9]*"
                        inputmode="numeric"
                        placeholder="Enter number of stocks"
                        value={stocks}
                        onChange={(e) => setStocks(e.target.value)}
                        required
                    />
                </Form.Group>
                <Form.Group controlId="image">
                    <Form.Label>Image URL</Form.Label>
                    <Form.Control type="text" onBlur={(e) => setImage(e.target.value)} />
                    <img src={image} alt="Product" />
                </Form.Group>
                {(stocks == 0 || name == "" || description == "" || price == "" || image == "") ?
                    <div className="d-flex justify-content-end">
                        <Button type="submit" variant="primary" onClick={handleSubmit} disabled>
                            Create Product
                        </Button>
                    </div>
                    :
                    <div className="d-flex justify-content-end">
                        <Button type="submit" variant="primary" onClick={handleSubmit}>
                            Create Product
                        </Button>
                    </div>
                }
            </Form>
        </div>
    );
}

export default CreateProduct;
