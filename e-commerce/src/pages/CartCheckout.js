import Loading from '../components/Loading';
import { useContext, useEffect, useState } from 'react';
import { Container, Row, Col, Button, Modal } from 'react-bootstrap';
import CartSummary from '../components/CartSummary';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
import { useNavigate, Navigate } from 'react-router-dom';

function formatCurrency(amount) {
    return new Intl.NumberFormat('en-PH', { style: 'currency', currency: 'PHP' }).format(amount);
}

function Cart() {
    const { user } = useContext(UserContext);
    const [cart, setCart] = useState([]);
    const [isLoading, setIsLoading] = useState(true);
    const [isModalOpen, setIsModalOpen] = useState(false);
    const navigate = useNavigate();
    const toggleModal = () => {
        setIsModalOpen(!isModalOpen);
    };

    const handleCheckout = () => {
        if (cart.products.length === 0) {
            // Show an error message if the cart is empty
            Swal.fire({
                title: 'Empty Cart',
                icon: 'warning',
                text: 'Your cart is empty.',
            });
        } else {
            // Navigate to the checkout page
            toggleModal()

        }
    };
    const checkout = () => {
        const token = localStorage.getItem('token');
        if (user.isAdmin === true) {
            Swal.fire({
                title: 'Unauthorize',
                icon: 'warning',
                text: 'Admins cannot purchase products.',
            });
            return;
        }

        fetch(`${process.env.REACT_APP_API_URL}/carts/cartCheckOut`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${token}`,
            },
        })
            .then((response) => response.json())
            .then((result) => {
                if (result) {
                    Swal.fire({
                        title: 'Success!',
                        icon: 'success',
                        text: 'Order checked out!',
                    });
                    navigate('/products');
                    setCart([]);
                } else {
                    console.log(result);

                    Swal.fire({
                        title: 'Something went wrong!',
                        icon: 'error',
                        text: 'Please try again',
                    });
                }
            })
            .catch((error) => {
                Swal.fire({
                    title: 'Error!',
                    icon: 'error',
                    text: 'Failed to checkout.',
                });
            });
    };

    const confirmCheckout = async () => {
        try {
            if (user.isAdmin === true) {
                Swal.fire({
                    title: 'Unauthorize',
                    icon: 'warning',
                    text: 'Admins cannot purchase products.',
                });
            }
            // do something with result here
            toggleModal(); // close the modal
            navigate('/cartCheckout');
        } catch (error) {
            // handle error
        }
    };
    useEffect(() => {
        console.log(user);
        const token = localStorage.getItem('token');

        fetch(`${process.env.REACT_APP_API_URL}/carts/userCart`, {
            headers: {
                Authorization: `Bearer ${token}`,
            },
        })
            .then((response) => response.json())
            .then((result) => {
                console.log(result);
                setCart(result);
                setIsLoading(false);
            })
            .catch((error) => {
                // handle error
            });
    }, []);

    return isLoading ? (
        <Loading />
    ) : (
        (user.id !== null && !user.isAdmin) ?
            <Container className='main-content-2'>
                <table className="table">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Price</th>
                            <th>Quantity</th>
                            <th>Total Price</th>
                        </tr>
                    </thead>
                    <tbody>
                        {cart.products.map((product) => (
                            <tr key={product.productId}>
                                <td>{product.name}</td>
                                <td>{formatCurrency(product.price)}</td>
                                <td>{product.quantity}</td>
                                <td>{formatCurrency(product.totalPrice)}</td>
                            </tr>
                        ))}
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colSpan="3">Total:</td>
                            <td>{formatCurrency(cart !== null && cart.totalAmount)}</td>
                        </tr>
                    </tfoot>
                </table>
                <div className="d-flex justify-content-end mb-3">
                    <Button onClick={handleCheckout}>Checkout Cart</Button>
                </div>
                <Modal show={isModalOpen} onHide={toggleModal}>
                    <Modal.Header closeButton>
                        <Modal.Title>Cart Summary</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <CartSummary cart={cart} />
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={toggleModal}>
                            Cancel
                        </Button>
                        <Button variant="primary" onClick={checkout}>
                            Proceed
                        </Button>
                    </Modal.Footer>
                </Modal>
            </Container>
            :
            null
    )
        ;
}
export default Cart;




