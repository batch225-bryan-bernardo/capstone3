import { useState, useEffect, useContext } from "react";
import { Button, Container, Form } from "react-bootstrap";
import Swal from "sweetalert2";
import { useNavigate, Navigate } from 'react-router-dom'
import UserContext from "../UserContext";


function Register() {
    const { user, setUser } = useContext(UserContext)
    const navigate = useNavigate()

    const [firstName, setFirstName] = useState('')
    const [lastName, setLastName] = useState('')
    const [email, setEmail] = useState('')
    const [mobileNumber, setMobileNumber] = useState('')
    const [password1, setPassword1] = useState('')
    const [password2, setPassword2] = useState('')
    const [isActive, setIsActive] = useState(false)


    function registerUser(event) {

        event.preventDefault()
        fetch(`${process.env.REACT_APP_API_URL}/users/checkUser`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email
            })
        })
            .then(response => response.json())
            .then(result => {
                if (result === true) {
                    Swal.fire({
                        title: 'Oops!',
                        icon: 'error',
                        text: 'Email already exist!'
                    })
                } else {
                    fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json'
                        },
                        body: JSON.stringify({
                            "firstName": firstName,
                            "lastName": lastName,
                            "email": email,
                            "mobileNo": mobileNumber,
                            "password": password1
                        })
                    }).then(response => response.json())
                        .then(result => {
                            if (typeof result !== "undefined") {
                                Swal.fire({
                                    title: 'Successfully Registered!',
                                    icon: 'success',
                                    text: 'Happy Shopping!'
                                })

                                navigate('/login')
                            } else {
                                Swal.fire({
                                    title: 'Registeration failed',
                                    icon: 'error',
                                    text: 'Invalid Email or password'
                                })
                            }
                        })
                }
            })
    }

    useEffect(() => {
        if ((firstName !== '' && lastName !== '' && mobileNumber.length === 11 && email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)) {
            // Enables the submit button if the form data has been verified
            setIsActive(true)
        } else {
            setIsActive(false)
        }
    }, [firstName, lastName, mobileNumber, email, password1, password2])


    return (
        user.id ?
            <Navigate to="/products" />
            :
            <Container className="log-container main-content align-items-center mt-4">
                <div className="register-form">
                    <Form onSubmit={event => registerUser(event)}>
                        <Form.Group >
                            <Form.Label>First Name</Form.Label>
                            <Form.Control
                                type="First Name"
                                placeholder="Enter First Name"
                                value={firstName}
                                onChange={event => setFirstName(event.target.value)}
                                required></Form.Control>
                        </Form.Group>
                        <Form.Group className=" mt-4">
                            <Form.Label>Last Name</Form.Label>
                            <Form.Control
                                type="Last Name"
                                placeholder="Enter Last Name"
                                value={lastName}
                                onChange={event => setLastName(event.target.value)}

                                required></Form.Control>
                        </Form.Group>
                        <Form.Group className=" mt-4">
                            <Form.Label>Email</Form.Label>
                            <Form.Control
                                type="email"
                                placeholder="Enter Email"
                                value={email}
                                onChange={event => setEmail(event.target.value)}

                                required></Form.Control>
                        </Form.Group>
                        <Form.Group className=" mt-4">
                            <Form.Label>Mobile Number</Form.Label>
                            <Form.Control
                                maxLength={11}
                                minLength={11}
                                type="mobileNumber"
                                placeholder="Enter Mobile Number"
                                value={mobileNumber}
                                onChange={event => setMobileNumber(event.target.value)}

                                required></Form.Control>
                        </Form.Group>
                        <Form.Group controlId="password1" className=" mt-4">
                            <Form.Label>Password</Form.Label>
                            <Form.Control
                                minLength={8}
                                type="password"
                                placeholder="Password"
                                value={password1}
                                onChange={event => setPassword1(event.target.value)}

                                required
                            />
                        </Form.Group>

                        <Form.Group controlId="password2" className=" mt-4">
                            <Form.Label>Confirm Password</Form.Label>
                            <Form.Control
                                type="password"
                                placeholder="Confirm Password"
                                value={password2}
                                onChange={event => setPassword2(event.target.value)}
                                required
                                className="mb-4"
                            />
                        </Form.Group>
                        {isActive ?
                            <Button variant="primary" type="submit" id="submitBtn">
                                Submit
                            </Button>
                            :
                            <Button variant="primary" type="submit" id="submitBtn" disabled>
                                Submit
                            </Button>
                        }
                    </Form>
                </div>
            </Container>
    )



}

export default Register;