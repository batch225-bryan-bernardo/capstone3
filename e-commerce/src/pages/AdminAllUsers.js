import React, { useState, useEffect, useContext } from "react";
import Swal from 'sweetalert2';
import { Accordion, Button, Form } from "react-bootstrap";
import UserContext from "../UserContext";

function AdminAllUsers() {
    const [users, setUsers] = useState([]);
    const { user } = useContext(UserContext)

    useEffect(() => {
        async function fetchUsers() {
            const token = localStorage.getItem('token');
            let response = await fetch(`${process.env.REACT_APP_API_URL}/users/allUsers`, {
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`
                },
            })
                .then(response => response.json())
                .then(data => {
                    if (data) {
                        console.log("Received data:", data);
                        const usersArray = {};
                        data.forEach(user => {
                            if (!usersArray[user._id]) {
                                usersArray[user._id] = [];
                            }
                            usersArray[user._id].push(user);
                        });
                        setUsers(Object.entries(usersArray));
                        Swal.fire({
                            title: "Success!",
                            icon: "success",
                            text: "Users Retrieved."
                        });
                    } else {
                        console.log(data);
                        Swal.fire({
                            title: "Something went wrong!",
                            icon: "error",
                            text: "Please try again"
                        });
                    }
                }).catch(error => {
                    console.error(error);
                    Swal.fire({
                        title: "Something went wrong!",
                        icon: "error",
                        text: "Please try again"
                    });
                });
        }
        fetchUsers();
    }, []);


    const toggleAdminStatus = async (userId, isAdmin) => {
        if (userId === user.id) {
            return;
        }
        const token = localStorage.getItem('token');
        const response = await fetch(`${process.env.REACT_APP_API_URL}/users/adminToggle`, {
            method: "PUT",
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${token}`
            },
            body: JSON.stringify({ userId: userId, isAdmin: !isAdmin }),
        });

        if (response.status === 200) {
            // Update the state with the updated user
            const updatedUsers = users.map(([id, user]) => {
                if (id === userId) {
                    return [id, [{ ...user[0], isAdmin: !isAdmin }]];
                } else {
                    return [id, user];
                }
            });
            setUsers(updatedUsers);

            Swal.fire({
                title: "Success!",
                icon: "success",
                text: "User admin status updated."
            });
        } else {
            Swal.fire({
                title: "Something went wrong!",
                icon: "error",
                text: "Please try again"
            });
        }
    };
    return (
        <div className="container">
            <h1 className="text-center mt-4">All Users</h1>
            <Accordion defaultActiveKey="0" flush>
                {users.map(([id, user]) => (
                    <Accordion.Item key={id} eventKey={id}>
                        <Accordion.Header>

                            <div className="d-flex align-items-center justify-content-between w-100">
                                <p>User Email: {user[0].email}</p>
                                {(user[0]._id !== user.id) ?
                                    <Form.Check
                                        type="switch"
                                        id={`custom-switch-${id}`}
                                        label={user[0].isAdmin ? "Admin" : "User"}
                                        checked={user[0].isAdmin}
                                        onChange={() => {
                                            if (id !== user.id) {
                                                toggleAdminStatus(id, user[0].isAdmin)
                                            }
                                        }}
                                        className="mx-2"
                                    />
                                    :
                                    null
                                }
                            </div>
                        </Accordion.Header>
                        <Accordion.Body>
                            <div className="card mb-2" key={user._id}>
                                <div className="card-header">
                                    Full Name: {user[0].firstName} {user[0].lastName}
                                    {user[0].isAdmin ? (
                                        <span className="ms-3 badge bg-success">Admin</span>
                                    ) : null}
                                </div>
                                <div className="card-body">
                                    <p>userId: {user[0]._id}</p>
                                    <p>Email: {user[0].email}</p>
                                </div>
                            </div>
                        </Accordion.Body>
                    </Accordion.Item>
                ))}
            </Accordion>
        </div >
    );

}

export default AdminAllUsers;
