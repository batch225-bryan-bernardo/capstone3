import Loading from '../components/Loading';
import { useContext, useEffect, useState } from 'react';
import { Container, Row, Col, Button, Modal } from 'react-bootstrap';
import CartSummary from '../components/CartSummary';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
import { useNavigate, Navigate } from 'react-router-dom';
import EditProductModal from '../components/EditProductModal';
import RemoveProductModal from '../components/RemoveProductModal';
import '../App.css'


function formatCurrency(amount) {
    return new Intl.NumberFormat('en-PH', { style: 'currency', currency: 'PHP' }).format(amount);
}

function Cart() {
    const { user } = useContext(UserContext);
    const [cart, setCart] = useState([]);
    const [currentProduct, setCurrentProduct] = useState()
    const [isLoading, setIsLoading] = useState(true);
    const [isModalOpen, setIsModalOpen] = useState(false);
    const navigate = useNavigate();
    const [showEditModal, setShowEditModal] = useState(false);
    const [showRemoveModal, setShowRemoveModal] = useState(false);
    const toggleModal = () => {
        setIsModalOpen(!isModalOpen);
    };
    const toggleModalEdit = () => {
        setShowEditModal(!showEditModal);
    };
    const toggleModalRemove = () => {
        setShowRemoveModal(!showEditModal);
    };

    const handleCheckout = () => {
        if (cart.products.length === 0) {
            // Show an error message if the cart is empty
            Swal.fire({
                title: 'Empty Cart',
                icon: 'warning',
                text: 'Your cart is empty.',
            });
        } else {
            // Navigate to the checkout page
            toggleModal()

        }
    };

    const confirmCheckout = async () => {
        try {
            if (user.isAdmin === true) {
                Swal.fire({
                    title: 'Unauthorize',
                    icon: 'warning',
                    text: 'Admins cannot purchase products.',
                });
            }
            // do something with result here
            toggleModal(); // close the modal
            navigate('/cartCheckout');
        } catch (error) {
            // handle error
        }
    };


    const handleEditProduct = (productId) => {
        const productIndex = cart.products.findIndex((product) => product.productId === productId);
        if (productIndex === -1) {
            return;
        }
        setCurrentProduct(cart.products[productIndex]);
        setShowEditModal(true); // add this line to show the modal
    };

    const handleRemoveProduct = (productId) => {
        const productIndex = cart.products.findIndex((product) => product.productId === productId);
        if (productIndex === -1) {
            return;
        }
        setCurrentProduct(cart.products[productIndex]);
        setShowRemoveModal(true); // add this line to show the modal
    };


    useEffect(() => {
        console.log(user);
        const token = localStorage.getItem('token');

        fetch(`${process.env.REACT_APP_API_URL}/carts/userCart`, {
            headers: {
                Authorization: `Bearer ${token}`,
            },
        })
            .then((response) => response.json())
            .then((result) => {
                console.log(result);
                setCart(result);
                setIsLoading(false);
            })
            .catch((error) => {
                // handle error
            });
    }, []);

    const userId = user.id
    console.log(cart.products)
    return isLoading ? (
        <Loading />
    ) : (
        <>
            {(userId !== null) ?
                <>
                    <Container className='main-content-2'>
                        {cart.products.length > 0 ? (
                            <table className="table">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>Name</th>
                                        <th>Price</th>
                                        <th>Quantity</th>
                                        <th>Total Price</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {cart.products.map((product) => (
                                        <tr key={product.productId}>
                                            <td> <img variant="top" src={product.image} /></td>
                                            <td>{product.name}</td>
                                            <td>{formatCurrency(product.price)}</td>
                                            <td>{product.quantity}</td>
                                            <td>{formatCurrency(product.totalPrice)}</td>
                                            <td>
                                                <Button variant="outline-secondary" onClick={() => handleEditProduct(product.productId)}>
                                                    Edit
                                                </Button>
                                                {showEditModal && (
                                                    <EditProductModal
                                                        product={product}
                                                        productId={product.productId}
                                                        handleEditProduct={handleEditProduct}
                                                        handleCloseModal={() => setShowEditModal(false)}
                                                    />
                                                )}
                                            </td>
                                            <td>
                                                <Button variant="outline-secondary " bg="danger" onClick={() => handleRemoveProduct(product.productId)}>
                                                    Remove
                                                </Button>
                                                {showRemoveModal && (
                                                    <RemoveProductModal
                                                        product={product}
                                                        productId={product.productId}
                                                        handleRemoveProduct={handleRemoveProduct}
                                                        handleCloseModal={() => setShowRemoveModal(false)}
                                                    />
                                                )}


                                            </td>
                                        </tr>
                                    ))}
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colSpan="3">Total:</td>
                                        <td>{formatCurrency(cart !== null && cart.totalAmount)}</td>
                                    </tr>
                                </tfoot>
                            </table>
                        ) : (
                            <p>No items in cart.</p>
                        )}
                        {cart.products.length > 0 && (
                            <div className='d-flex justify-content-end mb-2'>
                                <Button onClick={handleCheckout}>Proceed to Checkout</Button>
                            </div>
                        )}
                        <Modal show={isModalOpen} onHide={toggleModal}>
                            <Modal.Header closeButton>
                                <Modal.Title>Cart Summary</Modal.Title>
                            </Modal.Header>
                            <Modal.Body>
                                <CartSummary cart={cart} />
                            </Modal.Body>
                            <Modal.Footer>
                                <Button variant="secondary" onClick={toggleModal}>
                                    Cancel
                                </Button>
                                <Button variant="primary" onClick={confirmCheckout}>
                                    Proceed
                                </Button>
                            </Modal.Footer>
                        </Modal>
                    </Container>
                </>
                :
                navigate('/login')
            }
        </>
    );

}
export default Cart;