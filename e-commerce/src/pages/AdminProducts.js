import UserProductList from '../components/ProductDetails';
import AdminProductList from '../components/AdminProductDetails';
import Loading from '../components/Loading';
import { useContext, useEffect, useState } from 'react';
import { Container, Row, Col, Button } from 'react-bootstrap';
import UserContext from '../UserContext';
import FilterComponent from '../components/FilterComponent';
import { Link } from 'react-router-dom';

function formatCurrency(amount) {
    return new Intl.NumberFormat('en-PH', { style: 'currency', currency: 'PHP' }).format(amount);
}


function AdminProducts() {
    const [adminProducts, setAdminProducts] = useState([]);
    const [isLoading, setIsLoading] = useState(true);
    const { user } = useContext(UserContext);

    useEffect(() => {

        let token = localStorage.getItem('token');

        if (user && user.isAdmin) {
            fetch(`${process.env.REACT_APP_API_URL}/products/allProducts`, {
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`
                }
            })
                .then((response) => response.json())
                .then((result) => {
                    console.log(result)
                    setAdminProducts(result);
                    setIsLoading(false);
                })
                .catch((error) => {
                    console.log(error);
                    setIsLoading(false);
                });
        }
    }, [user]);

    return isLoading ? (
        <Loading />
    ) : (
        <div className="container">
            <FilterComponent />
            <h2>Products</h2>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th></th>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Price</th>
                        <th>Stocks</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    {adminProducts.map(product => (
                        <tr key={product._id}>
                            <td><img src={product.image} alt={product.name} width="100px" /></td>
                            <td>{product.name}</td>
                            <td>{product.description}</td>
                            <td>{formatCurrency(product.price)}</td>
                            <td>{product.stocks}</td>
                            <td><Button variant="primary" href={`/allProducts/${product._id}`}>Details</Button></td>
                        </tr>

                    ))}
                </tbody>
            </table>

        </div >
    );
}

export default AdminProducts;
