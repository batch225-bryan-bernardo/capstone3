import React from 'react';
import '../App.css'; // styles for the component
import LandingPageComponent from '../components/LandingPageComponent';
import HotProducts from '../components/HotProducts';
import HomeCarousel from '../components/HomeCarousel';


const HomePage = () => {
    return (
        <div className='main-home-page'>
            <div className='mt-4'>
                <LandingPageComponent />
                <div className='home-separator mt-4'>
                    <HomeCarousel />
                </div>
            </div>

            <HotProducts />
            <div className='hot-products-container'>

            </div>
        </div>
    );
}

export default HomePage;
