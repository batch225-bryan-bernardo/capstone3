import React, { useState, useEffect } from "react";
import { Button } from "react-bootstrap";
import Swal from 'sweetalert2'



function formatCurrency(amount) {
    return new Intl.NumberFormat('en-PH', { style: 'currency', currency: 'PHP' }).format(amount);
}


function UserOrders() {
    const [orders, setOrders] = useState([]);

    useEffect(() => {
        async function fetchOrders() {
            const token = localStorage.getItem('token')
            let response = await fetch(`${process.env.REACT_APP_API_URL}/users/userOrder`, {
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`
                },
            })
                .then(response => response.json())
                .then(data => {
                    if (data) {
                        console.log("Received data:", data);
                        setOrders(data);
                        Swal.fire({
                            title: "Success!",
                            icon: "success",
                            text: "Orders Retrieved."
                        });

                    } else {
                        console.log(data);
                        Swal.fire({
                            title: "Something went wrong!",
                            icon: "error",
                            text: "Please try again"
                        });
                    }
                }).catch(error => {
                    console.error(error);
                    Swal.fire({
                        title: "Something went wrong!",
                        icon: "error",
                        text: "Please try again"
                    });
                })
        }
        fetchOrders();
    }, []);

    const updateOrderStatus = async function (orderId, orderIndex) {

        const token = localStorage.getItem('token')
        const response = await fetch(`${process.env.REACT_APP_API_URL}/orders/updateOrderStatus/${orderId}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${token}`
            }
        });
        const data = await response.json();
        // update the order status in the local state
        setOrders(orders => {
            const newOrders = [...orders];
            newOrders[orderIndex].status = 'completed';
            newOrders[orderIndex].completedOn = new Date();
            return newOrders;
        });
    }



    return (
        <div className="container mx-auto">
            <h1 className="text-2xl font-bold mb-4">Orders</h1>
            {orders.map((order, index) => (
                <div key={order._id} className="border p-4 mb-4">
                    <div className="font-bold">Order ID: {order._id}</div>
                    <div>Ordered On: {new Date(order.orderedOn).toLocaleString()}</div>
                    <div>Total Amount: {formatCurrency(order.totalAmount.toFixed(2))}</div>
                    <div>Status: {order.status === 'completed' ?
                        <span className="text-green-600 font-bold" style={{ backgroundColor: "gray", border: "1px solid gray", color: "white" }}>Completed</span>
                        :
                        <Button variant="primary" disabled={order.completed} onClick={() => updateOrderStatus(order._id, index)}>Mark as Completed</Button>
                    }</div>
                    <div>Completed On: {order.status === 'completed' ?
                        (order.completedOn ? new Date(order.completedOn).toLocaleString() : 'N/A')
                        :
                        'N/A'
                    }</div>
                    <div className="mt-2">
                        Products:
                        {order.products.map((product) => (
                            <div key={product._id}>
                                <div>____________________________</div>
                                <div>Product ID: {product.productId}</div>
                                <div>Name: {product.name}</div>
                                <div>Quantity: {product.quantity}</div>
                                <div>Price: {formatCurrency(product.price.toFixed(2))}</div>
                                <div>Total Price: {formatCurrency(product.totalPrice.toFixed(2))}</div>

                            </div>

                        ))}
                    </div>
                </div>
            ))}
        </div>
    );


}

export default UserOrders;
