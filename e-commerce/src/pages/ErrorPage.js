import brand from '../imgs/kingbear.png'

export default function ErrorPage() {

    return (
        <div className='main-content-2'>
            <h2>Polar Sports & Fitness Co.</h2>
            <h1>Page Not Found</h1>
            <p>Go back to <a href="/">homepage</a>.</p>
            <img src={brand} />
        </div>
    )


}