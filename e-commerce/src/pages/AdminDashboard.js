import React from 'react';
import { Button, Card, Col, Row } from 'react-bootstrap';
import { Link } from 'react-router-dom'
import '../App.css';

function AdminDashboard() {

    return (
        <div className="admin-dashboard">
            <h1>Admin Dashboard</h1>
            <Row className="admin-card-row">
                <Col>
                    <Card>
                        <Card.Body>
                            <Card.Title>Users</Card.Title>
                            <Card.Text>
                                View and manage user accounts
                            </Card.Text>
                            <Link to="/allUsers"><Button variant="primary">Go to Users</Button></Link>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
            <Row className="admin-card-row">
                <Col>
                    <Card>
                        <Card.Body>
                            <Card.Title>Products</Card.Title>
                            <Card.Text>
                                Go to products
                            </Card.Text>
                            <Link to="/allProducts"><Button variant="primary" >Go to Products</Button></Link>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
            <Row className="admin-card-row">
                <Col>
                    <Card>
                        <Card.Body>
                            <Card.Title>Orders</Card.Title>
                            <Card.Text>
                                View and manage orders
                            </Card.Text>
                            <Link to="/orders"> <Button variant="primary">Go to Orders</Button></Link>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
            <Row className="admin-card-row">
                <Col>
                    <Card>
                        <Card.Body>
                            <Card.Title>Create Product</Card.Title>
                            <Card.Text>
                                Add a new product to the store
                            </Card.Text>
                            <Link to="/createProducts"><Button variant="primary" >Create Product</Button></Link>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>

        </div >
    );
}

export default AdminDashboard;
