import { useContext, useEffect, useState } from 'react';
import { Container, Button } from 'react-bootstrap';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
import { useNavigate, useParams } from 'react-router-dom';
import CartSummary from '../components/CartSummary';

function formatCurrency(amount) {
    return new Intl.NumberFormat('en-PH', { style: 'currency', currency: 'PHP' }).format(amount);
}
function BuyNowCheckout() {
    const { productId } = useParams();
    const { user } = useContext(UserContext);
    const navigate = useNavigate();
    const [product, setProduct] = useState(null);
    const [qty, setQty] = useState(1)
    const token = localStorage.getItem('token');

    const checkout = () => {

        const token = localStorage.getItem('token');
        if (user.isAdmin === true) {
            Swal.fire({
                title: 'Unauthorize',
                icon: 'warning',
                text: 'Admins cannot purchase products.',
            });
            return;
        }
        fetch(`${process.env.REACT_APP_API_URL}/orders/checkOut`, {
            method: 'POST',
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${token}`
            },
            body: JSON.stringify({
                products:
                    { "productId": productId, quantity: qty }
            })
        })
            .then(response => response.json())
            .then(result => {
                if (result) {

                    Swal.fire({
                        title: "Success!",
                        icon: "success",
                        text: "Order checked out!"
                    })

                    navigate('/products')
                } else {
                    console.log(result)

                    Swal.fire({
                        title: "Something went wrong!",
                        icon: "error",
                        text: "Please try again"
                    })
                }
            })
            .catch((error) => {
                Swal.fire({
                    title: 'Error!',
                    icon: 'error',
                    text: 'Failed to checkout.',
                });
            });
    };


    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
            .then(response => response.json())
            .then(result => {
                setProduct(result);
            });
    }, [productId]);


    return (
        (user.id !== null && !user.isAdmin) ?
            <Container className='main-content-2 align-items'>
                {product && (
                    <>
                        <h1>{product.name}</h1>
                        <img src={product.image} />
                        <p>{product.description}</p>
                        <p>{formatCurrency(product.price)}</p>
                        <div className="d-flex justify-content-end mb-3">
                            <Button onClick={checkout}>Checkout</Button>
                        </div>
                    </>
                )}
            </Container>
            :
            null
    );
}

export default BuyNowCheckout;
