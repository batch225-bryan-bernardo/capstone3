import UserProductList from '../components/ProductDetails';
import AdminProductList from '../components/AdminProductDetails';
import Loading from '../components/Loading';
import { useContext, useEffect, useState } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import UserContext from '../UserContext';
import FilterComponent from '../components/FilterComponent';
import { Pagination } from 'react-bootstrap';
function Products() {
    const [products, setProducts] = useState([]);
    const [adminProducts, setAdminProducts] = useState([]);
    const [isLoading, setIsLoading] = useState(true);
    const { user } = useContext(UserContext);

    useEffect(() => {

        let token = localStorage.getItem('token');

        fetch(`${process.env.REACT_APP_API_URL}/products`)
            .then((response) => response.json())
            .then((result) => {
                console.log(result)
                setProducts(result);
                setIsLoading(false);
            })
            .catch((error) => {
                console.log(error);
                setIsLoading(false);
            });

    }, [user]);
    return isLoading ? (
        <Loading />
    ) : (

        <Container>
            <FilterComponent />
            <h2 className='products-text'>Products</h2>
            <Row className="mx-auto text-center">
                {products.map(product => (
                    <Col key={product._id} xs={12} sm={6} md={4} lg={3} xl={4}>
                        <UserProductList productProp={product} />
                    </Col>
                ))}
            </Row>
        </Container>

    );
}

export default Products;
