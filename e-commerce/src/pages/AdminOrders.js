import React, { useState, useEffect } from "react";
import Swal from 'sweetalert2';
import { Accordion } from "react-bootstrap";


function formatCurrency(amount) {
    return new Intl.NumberFormat('en-PH', { style: 'currency', currency: 'PHP' }).format(amount);
}

function AdminOrders() {
    const [orders, setOrders] = useState([]);

    useEffect(() => {
        async function fetchOrders() {
            const token = localStorage.getItem('token');
            let response = await fetch(`${process.env.REACT_APP_API_URL}/orders/allOrders`, {
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`
                },
            })
                .then(response => response.json())
                .then(data => {
                    if (data) {
                        console.log("Received data:", data);
                        const ordersByUser = {};
                        data.forEach(order => {
                            if (!ordersByUser[order.userId]) {
                                ordersByUser[order.userId] = [];
                            }
                            ordersByUser[order.userId].push(order);
                        });
                        setOrders(Object.entries(ordersByUser));
                        Swal.fire({
                            title: "Success!",
                            icon: "success",
                            text: "Orders Retrieved."
                        });
                    } else {
                        console.log(data);
                        Swal.fire({
                            title: "Something went wrong!",
                            icon: "error",
                            text: "Please try again"
                        });
                    }
                }).catch(error => {
                    console.error(error);
                    Swal.fire({
                        title: "Something went wrong!",
                        icon: "error",
                        text: "Please try again"
                    });
                });
        }
        fetchOrders();
    }, []);

    return (
        <div className="container">
            <h1 className="text-center mt-4">All Orders</h1>
            {orders.length > 0 ? (
                <Accordion defaultActiveKey="0" flush>
                    {orders.map(([email, userOrders]) => (
                        <Accordion.Item key={email} eventKey={email}>
                            <Accordion.Header>
                                <h2>User Email: {userOrders[0].email}</h2>
                            </Accordion.Header>
                            <Accordion.Body>
                                {userOrders.map((order) => (
                                    <div className="card mb-2" key={order._id}>
                                        <div className="card-header">Order ID: {order._id}</div>
                                        <div className="card-body">
                                            <p>
                                                Ordered On:{" "}
                                                {new Date(order.orderedOn).toLocaleString()}
                                            </p>
                                            <p>Total Amount: {formatCurrency(order.totalAmount.toFixed(2))}</p>
                                            <ul>
                                                {order.products.map((product) => (
                                                    <li key={product._id}>
                                                        {product.name} ({product.quantity}) - $
                                                        {product.totalPrice.toFixed(2)}
                                                    </li>
                                                ))}
                                            </ul>
                                        </div>
                                    </div>
                                ))}
                            </Accordion.Body>
                        </Accordion.Item>
                    ))}
                </Accordion>
            ) : (
                <p>Loading orders...</p>
            )}
        </div>
    );
}

export default AdminOrders;