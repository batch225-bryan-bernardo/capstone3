import { useState } from 'react';
import { Button, Form, Modal } from 'react-bootstrap';
import Swal from 'sweetalert2';

function EditProductModal({ product, productId, handleEditProduct, handleCloseModal }) {

    const [quantity, setQuantity] = useState(product.quantity);

    const handleEdit = () => {
        if (quantity <= 0) {
            Swal.fire({
                icon: 'error',
                title: 'Invalid quantity',
                text: 'The quantity must be greater than 0.',
            });
            return;
        }

        const editedProduct = {
            newQuantity: quantity,
        };
        console.log(product.productId);
        const token = localStorage.getItem('token');

        fetch(`${process.env.REACT_APP_API_URL}/carts/updateQuantity/${productId}?_=${Math.random()}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${token}`,
            },
            body: JSON.stringify(editedProduct),
        })
            .then((response) => response.json())
            .then((result) => {
                console.log(result);
                handleEditProduct(productId);
                handleCloseModal();
                Swal.fire({
                    icon: 'success',
                    title: 'Product Updated',
                    text: 'The product quantity has been updated successfully.',
                });
            })
            .catch((error) => {
                console.error(error);
                Swal.fire({
                    icon: 'error',
                    title: 'Error',
                    text: 'An error occurred while updating the product quantity.',
                });
            })
    };

    return (
        <Modal show={true} onHide={handleCloseModal}>
            <Modal.Header closeButton>
                <Modal.Title>Edit Product</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Form>
                    <Form.Group>
                        <Form.Label>Name: {product.name}</Form.Label>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Price: {product.price}</Form.Label>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Quantity:</Form.Label>
                        <Form.Control type="number" value={quantity} onChange={(e) => setQuantity(e.target.value)} />
                    </Form.Group>
                </Form>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={handleCloseModal}>
                    Cancel
                </Button>
                <Button variant="primary" onClick={handleEdit}>
                    Save Changes
                </Button>
            </Modal.Footer>
        </Modal>
    );
}

export default EditProductModal;
