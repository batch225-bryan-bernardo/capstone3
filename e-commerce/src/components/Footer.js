import React from 'react';
import '../App.css'

const Footer = () => {
    return (
        <footer className='footer'>
            <div className="container">
                <div className="row">
                    <div className="col-md-4">
                        <h4>Contacts</h4>
                        <ul className="list-unstyled">
                            <li>Address: 123 Brgy. Tago, Dimahanap City</li>
                            <li>Phone: (123) 456-7890</li>
                            <li>Email: thepolarsports@example.com</li>
                        </ul>
                    </div>
                    <div className="col-md-4">
                        <h4>About</h4>
                        <p>The Polar Sports Health & Fitness Co. Is a company that is dedicated to serve and improve people's lives.</p>
                    </div>
                    <div className="col-md-4">
                        <h4>Copyright</h4>
                        <p>&copy; 2023 The Polar Sports Health & Fitness Company. All rights reserved.</p>
                    </div>
                </div>
            </div>
        </footer>
    );
};

export default Footer;