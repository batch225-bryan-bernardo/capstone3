import HotProductDetails from '../components/HotProductDetails';
import AdminProductList from '../components/AdminProductDetails';
import Loading from '../components/Loading';
import { useContext, useEffect, useState } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import UserContext from '../UserContext';
import Pagination from 'react-bootstrap/Pagination';

function HotProducts() {
    const [products, setProducts] = useState([]);
    const [adminProducts, setAdminProducts] = useState([]);
    const [isLoading, setIsLoading] = useState(true);
    const [currentPage, setCurrentPage] = useState(1);
    const productsPerPage = 12; // Change this to the desired number of products per page
    const { user } = useContext(UserContext);

    useEffect(() => {

        let token = localStorage.getItem('token');

        fetch(`${process.env.REACT_APP_API_URL}/products`)
            .then((response) => response.json())
            .then((result) => {
                console.log(result)
                setProducts(result);
                setIsLoading(false);
            })
            .catch((error) => {
                console.log(error);
                setIsLoading(false);
            });

    }, [user]);

    const indexOfLastProduct = currentPage * productsPerPage;
    const indexOfFirstProduct = indexOfLastProduct - productsPerPage;
    const currentProducts = products.slice(indexOfFirstProduct, indexOfLastProduct);

    const pageNumbers = [];
    for (let i = 1; i <= Math.ceil(products.length / productsPerPage); i++) {
        pageNumbers.push(i);
    }

    const handleClick = (event) => {
        setCurrentPage(Number(event.target.id));
    };

    return isLoading ? (
        <Loading />
    ) : (
        <Container>
            <h2 className='d-flex justify-content-center'>Hot Products</h2>
            <Row className="mx-auto text-center d-flex justify-content-center">
                {currentProducts.map(product => (
                    <Col key={product._id} xs={12} sm={6} md={4} lg={3} xl={3}>
                        <HotProductDetails productProp={product} />
                    </Col>
                ))}
            </Row>
            <Row>
                <Pagination className="align-items-center mx-auto d-flex justify-content-center">
                    <Pagination.First onClick={() => setCurrentPage(1)} disabled={currentPage === 1} />
                    <Pagination.Prev onClick={() => setCurrentPage(currentPage - 1)} disabled={currentPage === 1} />
                    {pageNumbers.map(number => (
                        <Pagination.Item key={number} id={number} active={number === currentPage} onClick={handleClick}>
                            {number}
                        </Pagination.Item>
                    ))}
                    <Pagination.Ellipsis />
                    <Pagination.Item>{pageNumbers.length}</Pagination.Item>
                    <Pagination.Next onClick={() => setCurrentPage(currentPage + 1)} disabled={currentPage === pageNumbers.length} />
                    <Pagination.Last onClick={() => setCurrentPage(pageNumbers.length)} disabled={currentPage === pageNumbers.length} />
                </Pagination>
            </Row>
        </Container>
    );
}

export default HotProducts;
