import React, { useState } from 'react';
import { Form } from 'react-bootstrap';

function FilterComponent({ filterProducts }) {
    const [searchTerm, setSearchTerm] = useState('');
    const [category, setCategory] = useState('');

    const handleSearchChange = (event) => {
        setSearchTerm(event.target.value);
        filterProducts(event.target.value, category);
    };

    const handleCategoryChange = (event) => {
        setCategory(event.target.value);
        filterProducts(searchTerm, event.target.value);
    };

    return (
        <Form className="mb-4">
            <Form.Group controlId="search">
                <Form.Control
                    type="text"
                    placeholder="Search for products..."
                    value={searchTerm}
                    onChange={handleSearchChange}
                />
            </Form.Group>
            <Form.Group controlId="category">
                <Form.Control as="select" value={category} onChange={handleCategoryChange}>
                    <option value="">All Categories</option>
                    <option value="Sports Wear">Sports Wear</option>
                    <option value="Shoes">Shoes</option>
                    <option value="Gym Equipment">Gym Equipment</option>
                    <option value="gym accesories">Gym Accessories</option>
                </Form.Control>
                <div className="dropdown-arrow">
                    <i class="bi bi-caret-down-fill"></i>
                </div>
            </Form.Group>

        </Form>
    );
}

export default FilterComponent;
