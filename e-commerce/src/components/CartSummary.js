import { Container, Row, Col } from 'react-bootstrap';



function formatCurrency(amount) {
    return new Intl.NumberFormat('en-PH', { style: 'currency', currency: 'PHP' }).format(amount);
}

function CartSummary({ cart }) {
    const formatter = new Intl.NumberFormat('en-PH', {
        style: 'currency',
        currency: 'PHP',
        minimumFractionDigits: 2,
        maximumFractionDigits: 2,
    });

    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Product</th>
                    <th>Price</th>
                    <th>Quantity</th>
                    <th>Total</th>
                </tr>
            </thead>
            <tbody>
                {cart.products.map((product) =>
                    <tr key={product.productId}>
                        <td>{product.name}</td>
                        <td>{formatter.format(product.price)}</td>
                        <td>{product.quantity.toLocaleString()}</td>
                        <td>{formatter.format(product.totalPrice)}</td>
                    </tr>

                )}
                <tr>
                    <td colSpan="3" className="text-right">
                        Total:
                    </td>
                    <td colSpan="3">Total:</td>
                    <td>{formatCurrency(cart !== null && cart.totalAmount)}</td>
                </tr>
            </tbody>
        </table>
    );
}

export default CartSummary;
