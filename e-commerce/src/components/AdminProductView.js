import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col, CardImg, Stack } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'



function formatCurrency(amount) {
    return new Intl.NumberFormat('en-PH', { style: 'currency', currency: 'PHP' }).format(amount);
}


export default function AdminProductView() {

    // Gets the courseId from the URL of the route that this component is connected to. '/courses/:courseId'
    const { productId } = useParams()
    const { user } = useContext(UserContext)
    const token = localStorage.getItem('token')
    const [name, setName] = useState("");
    const [description, setDescription] = useState("");
    const [price, setPrice] = useState(0);
    const [image, setImage] = useState("");
    const [status, setStatus] = useState('')
    const [stocks, setStocks] = useState(0)

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/products/singleProduct/${productId}`, {
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${token}`
            }
        })
            .then(response => {
                if (!response.ok) {
                    throw new Error('Failed to fetch product');
                }
                return response.json();
            })
            .then(result => {
                setName(result.name)
                setDescription(result.description)
                setPrice(result.price)
                setStatus(result.isActive)
                setStocks(result.stocks)
                setImage(result.image)
            })
            .catch(error => {
                console.error(error);
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Failed to fetch product!'
                });
            });
    }, [productId])

    const archiveProduct = () => {
        fetch(`${process.env.REACT_APP_API_URL}/products/archiveProduct/${productId}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${token}`
            }
        }).then(response => response.json())
            .then((result) => {
                setStatus(false);
            })
    }

    const unArchiveProduct = () => {
        fetch(`${process.env.REACT_APP_API_URL}/products/unArchiveProduct/${productId}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${token}`
            }
        }).then(response => response.json())
            .then((result) => {
                setStatus(true);
            })
    }


    console.log(status)
    return (
        <>
            {(user.id !== null && user.isAdmin === true) ?
                <Container className="mt-5 text-center" >
                    <Row>
                        <Col lg={{ span: 6, offset: 3 }}>
                            <Card  >
                                <Card.Body >
                                    <Card.Title>{name}</Card.Title>
                                    <Card.Subtitle>Description:</Card.Subtitle>
                                    <Card.Text>{description}</Card.Text>
                                    <Card.Subtitle>Price:</Card.Subtitle>
                                    <Card.Text>{formatCurrency(price)}</Card.Text>
                                    <Card.Text>Stocks: {stocks}</Card.Text>
                                    <CardImg top src={image} />
                                    <Stack className="align-items-center">
                                        {status
                                            ? <Button className='w-25 btn-block' variant="danger" onClick={archiveProduct}>Archive</Button>
                                            :
                                            <Button className='w-25 btn-block' variant="primary" onClick={unArchiveProduct}>Unarchive</Button>
                                        }
                                    </Stack>
                                    <Stack className="align-items-center mt-2">
                                        <Link to={`/updateProducts/${productId}`}><Button >Update Product</Button></Link>
                                    </Stack>
                                </Card.Body>
                            </Card>
                        </Col>
                    </Row>
                </Container>
                :
                null
            }
        </>
    )
}