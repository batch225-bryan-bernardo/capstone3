import { useState, useEffect, useContext } from 'react';
import { Modal, Container, Card, Button, Row, Col, CardImg, Stack } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'

function formatCurrency(amount) {
    return new Intl.NumberFormat('en-PH', { style: 'currency', currency: 'PHP' }).format(amount);
}

export default function ProductView() {

    const { productId } = useParams()
    const { user } = useContext(UserContext)
    const navigate = useNavigate()
    const [name, setName] = useState("");
    const [description, setDescription] = useState("");
    const [price, setPrice] = useState(0);
    const [image, setImage] = useState();
    const [qty, setQty] = useState(1)
    const [showModal, setShowModal] = useState(false)

    const addQty = () => {
        if (qty >= 0)
            setQty(qty + 1)

    }

    const subQty = () => {
        if (qty > 0)
            setQty(qty - 1)

    }
    const handleShow = () => setShowModal(true);
    const handleClose = () => setShowModal(false);

    console.log(user)
    const buyNow = () => {
        if (user.isAdmin === true) {
            Swal.fire({
                title: "Unauthorize",
                icon: "warning",
                text: "Admins cannot purchase products."
            });
        } else {
            setShowModal(true)
        }
    };

    const addToCart = (productId) => {
        if (user.isAdmin === true) {
            Swal.fire({
                title: "Unauthorize",
                icon: "warning",
                text: "Admins cannot purchase products."
            })
        } else {
            fetch(`${process.env.REACT_APP_API_URL}/carts/addToCart`, {
                method: 'POST',
                headers: {
                    "Content-Type": "application/json",
                    Authorization: `Bearer ${localStorage.getItem('token')
                        } `
                },
                body: JSON.stringify({
                    products:
                        { "productId": productId, quantity: qty }
                })
            })
                .then(response => response.json())
                .then(result => {
                    if (result) {
                        Swal.fire({
                            title: "Success!",
                            icon: "success",
                            text: "Product added to cart!"
                        })

                        navigate('/products')
                    } else {
                        console.log(result)

                        Swal.fire({
                            title: "Something went wrong!",
                            icon: "error",
                            text: "Please try again"
                        })
                    }
                })
        }
    }

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/products/${productId} `)
            .then(response => response.json())
            .then(result => {
                console.log(result.name)
                console.log(result.price)
                console.log(result.image)
                setName(result.name)
                setDescription(result.description)
                setPrice(result.price)
                setImage(result.image)
            })
    }, [productId])
    console.log(productId)
    return (
        <>
            <Button variant="primary" onClick={handleShow}>
                View Product
            </Button>

            <Modal show={showModal} onHide={handleClose} size="lg">

                <Modal.Header closeButton>
                    <Modal.Title>{name}</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Row>
                        <Col lg={{ span: 6 }}>
                            <Card.Img src={image} />
                        </Col>
                        <Col lg={{ span: 6 }}>
                            <Card.Subtitle>Description:</Card.Subtitle>
                            <Card.Text>{description}</Card.Text>
                            <Card.Subtitle>Price:</Card.Subtitle>
                            <Card.Text>{formatCurrency(price)}</Card.Text>
                            <Stack className="align-items-center">
                                {qty !== 0 ? (
                                    <Button
                                        className="w-25 btn-block"
                                        variant="primary"
                                        onClick={() => buyNow(productId)}
                                    >
                                        Buy Now
                                    </Button>
                                ) : (
                                    <Button
                                        className="w-25 btn-block"
                                        variant="primary"
                                        onClick={() => buyNow(productId)}
                                        disabled
                                    >
                                        Buy Now
                                    </Button>
                                )}
                                <Button className="w-25 btn-block" variant="primary" onClick={addQty}>
                                    +
                                </Button>
                                <span>{qty}</span>
                                {qty !== 0 ? (
                                    <Button className="w-25 btn-block" variant="primary" onClick={subQty}>
                                        -
                                    </Button>
                                ) : (
                                    <Button
                                        className="w-25 btn-block"
                                        variant="primary"
                                        onClick={subQty}
                                        disabled
                                    >
                                        -
                                    </Button>
                                )}
                                {qty !== 0 ? (
                                    <Button
                                        className="w-50 btn-block"
                                        variant="primary"
                                        onClick={() => addToCart(productId)}
                                    >
                                        Add to Cart
                                    </Button>
                                ) : (
                                    <Button
                                        className="w-50 btn-block"
                                        variant="primary"
                                        onClick={() => addToCart(productId)}
                                        disabled
                                    >
                                        Add to Cart
                                    </Button>
                                )}
                            </Stack>
                        </Col>
                    </Row>
                </Modal.Body>
                <Modal.Footer>
                    {user.id !== null ? (
                        <Button variant="secondary" onClick={handleClose}>
                            Close
                        </Button>
                    ) : (
                        <Link className="btn btn-success" to="/login">
                            Login to proceed
                        </Link>
                    )}
                </Modal.Footer>
            </Modal>
        </>
    );
}