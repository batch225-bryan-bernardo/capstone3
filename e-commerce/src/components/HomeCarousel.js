import Carousel from 'react-bootstrap/Carousel';
import polarBanner from '../imgs/polarBanner.PNG'
import GirlWorkout from '../imgs/girlworkout.jpg'



function HomeCarousel() {
    return (
        <div className="text-center">
            <div >
                <Carousel fade>
                    <Carousel.Item className="carousel-items">
                        <img src={polarBanner} alt="First slide" />
                    </Carousel.Item>
                    <Carousel.Item className="carousel-items">
                        <img
                            src="https://images8.alphacoders.com/628/628884.jpg"
                            alt="Second slide"
                            className='w-50'
                        />
                    </Carousel.Item>
                    <Carousel.Item className="carousel-items">
                        <img
                            src={GirlWorkout}
                            alt="Third slide"
                            className='w-50'
                        />
                    </Carousel.Item>
                </Carousel>
            </div>
        </div>
    );
}

export default HomeCarousel;
