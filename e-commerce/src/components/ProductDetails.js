import { Button, Card } from 'react-bootstrap';
import { CardImg } from 'react-bootstrap';
import { Link } from 'react-router-dom'
import { Container, Row, Col, Modal } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom'
import { useContext, useState } from 'react';
import UserContext from '../UserContext'
import Swal from 'sweetalert2'
import ListGroup from 'react-bootstrap/ListGroup';

function formatCurrency(amount) {
    return new Intl.NumberFormat('en-PH', { style: 'currency', currency: 'PHP' }).format(amount);
}

function UserProductList(product) {
    const [isModalOpen, setIsModalOpen] = useState(false);

    const toggleModal = () => {
        setIsModalOpen(!isModalOpen);
    }
    const handleCheckout = () => {
        toggleModal();
    }


    const { user } = useContext(UserContext)
    const navigate = useNavigate()

    const buyNow = () => {
        if (user.isAdmin === true) {
            Swal.fire({
                title: "Unauthorize",
                icon: "warning",
                text: "Admins cannot purchase products."
            });
        } else {
            navigate(`/checkout/${_id}`);
        }
    };

    // Destructuring the props
    const { name, description, price, image, _id, stocks, createdOn } = product.productProp

    return (
        <>

            <Col key={product._id}>
                <Card className="rounded-lg fixed-card ">
                    <Card.Img variant="top" src={image} />
                    <Card.Body>
                        <Card.Title>{name}</Card.Title>
                    </Card.Body>
                    <ListGroup className="list-group-flush">
                        <ListGroup.Item>{description}</ListGroup.Item>
                        <ListGroup.Item>{formatCurrency(price)}</ListGroup.Item>
                        {(stocks !== 0) ?
                            <ListGroup.Item>Stocks Left: {stocks}</ListGroup.Item>
                            :
                            <ListGroup.Item>Not Available</ListGroup.Item>
                        }
                    </ListGroup>
                    <Card.Body className='d-flex justify-content-center flex-wrap align-items-center card-details-buy'>
                        <Card.Link href={`/products/${_id}`}><Button className='w-150 mb-1 mb-md-0' >Details</Button></Card.Link>
                        {(user.id !== null) ?
                            <Card.Link><Button className='w-150' variant="primary" onClick={buyNow}>Buy now!</Button></Card.Link>
                            :
                            <Card.Link ><Button className='w-150' variant="primary" onClick={() => navigate("/login")}>Login to Buy!</Button></Card.Link>
                        }
                    </Card.Body>
                </Card>
            </Col>

        </>
    )

}



export default UserProductList;