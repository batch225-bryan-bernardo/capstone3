import React, { useState } from 'react';
import { Modal, Button } from 'react-bootstrap';

function EditProductModal({ product, handleEditProduct, handleCloseModal }) {
    const [quantity, setQuantity] = useState(product.quantity);

    const handleSubmit = () => {
        handleEditProduct(product.productId, quantity);
        handleCloseModal();
    };

    const handleQuantityChange = (event) => {
        setQuantity(event.target.value);
    };

    return (
        <Modal show={true} onHide={handleCloseModal}>
            <Modal.Header closeButton>
                <Modal.Title>Edit Product</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <form>
                    <div className="mb-3">
                        <label className="form-label">Name: {product.name}</label>
                    </div>
                    <div className="mb-3">
                        <label className="form-label">Price: {product.price}</label>
                    </div>
                    <div className="mb-3">
                        <label className="form-label">Quantity:</label>
                        <input type="number" className="form-control" value={quantity} onChange={handleQuantityChange} />
                    </div>
                    <div className="mb-3">
                        <label className="form-label">Total Price: {product.totalPrice}</label>
                    </div>
                </form>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={handleCloseModal}>
                    Cancel
                </Button>
                <Button variant="primary" onClick={handleSubmit}>
                    Save Changes
                </Button>
            </Modal.Footer>
        </Modal>
    );
}

export default EditProductModal;