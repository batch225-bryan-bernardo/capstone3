import { Button, Card } from 'react-bootstrap';
import { CardImg } from 'react-bootstrap';
import { Link } from 'react-router-dom'
import { Container, Row, Col, Modal } from 'react-bootstrap';
import { useParams, useNavigate } from 'react-router-dom'
import { useContext, useState } from 'react';
import UserContext from '../UserContext'
import Swal from 'sweetalert2'
import ListGroup from 'react-bootstrap/ListGroup';


function formatCurrency(amount) {
    return new Intl.NumberFormat('en-PH', { style: 'currency', currency: 'PHP' }).format(amount);
}
function AdminProductList(product) {
    const [isModalOpen, setIsModalOpen] = useState(false);

    const toggleModal = () => {
        setIsModalOpen(!isModalOpen);
    }
    const handleCheckout = () => {
        toggleModal();
    }


    const { user } = useContext(UserContext)
    const navigate = useNavigate()


    const buyNow = () => {
        if (user.isAdmin === true) {
            Swal.fire({
                title: "Unauthorize",
                icon: "warning",
                text: "Admins cannot purchase products."
            })
        }
        navigate("/checkout")
    }
    // Destructuring the props
    const { name, description, price, image, _id, stocks } = product.adminProductProp
    return (

        <Col key={product._id}>
            <Card className="rounded-lg fixed-card ">
                <img variant="top" src={image} />
                <div className="card-body">
                    <Card.Body>
                        <Card.Title>{name}</Card.Title>
                    </Card.Body>
                    <ListGroup className="list-group-flush">
                        <ListGroup.Item>{description}</ListGroup.Item>
                        <ListGroup.Item>{formatCurrency(price)}</ListGroup.Item>
                        <ListGroup.Item>Stocks: {stocks}</ListGroup.Item>
                    </ListGroup>
                    <Card.Body className='d-flex justify-content-center flex-wrap'>
                        <Link className="btn btn-primary" to={`/allProducts/${_id}`}>Details</Link>
                    </Card.Body>
                </div>
            </Card>
        </Col >

    )
}


export default AdminProductList;
