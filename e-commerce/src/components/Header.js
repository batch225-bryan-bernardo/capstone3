import BrandLogo from '../imgs/ThePolarRun.png';
import { useState, useEffect } from 'react';
import { Navbar, Nav, Form, FormControl, Button } from 'react-bootstrap';
import '../App.css';
import { FaSearch } from 'react-icons/fa';

function Header() {
    const [isMobile, setIsMobile] = useState(false);

    // add a resize listener to detect when the screen width changes
    useEffect(() => {
        function handleResize() {
            setIsMobile(window.innerWidth < 640);
        }

        window.addEventListener('resize', handleResize);
        handleResize(); // initialize state on first load
        return () => window.removeEventListener('resize', handleResize);
    }, []);
    return (
        <>
            <div className="main-logo-page">
                <div className="container" fluid>
                    <div className="row justify-content-center align-items-center">
                        <div className="col-lg-10 d-flex align-items-center">
                            {!isMobile && (
                                <img
                                    src={BrandLogo}
                                    width={300}
                                    height={300}
                                    className="d-inline-block align-top align-items-center"
                                    alt="Logo"
                                />
                            )}
                            <div>
                                {/* hide the h3 element on mobile devices */}
                                <h3 className='company-name ms-5'>
                                    The Polar Health & Fitness Co.
                                </h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );

}

export default Header;
