import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container';
import Form from 'react-bootstrap/Form';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown';

import { Link, NavLink } from 'react-router-dom';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import BrandLogo from '../imgs/polarlogo.png';
import { FaSearch, FaShoppingCart, FaUser } from 'react-icons/fa';
import '../App.css'


function AppNavbar() {
    const { user } = useContext(UserContext);


    return (
        <>

            <Navbar className="mainNav" expand="lg" bg="dark" variant="dark">
                <Container fluid>
                    <Navbar.Toggle aria-controls="navbarScroll" />
                    <Navbar.Collapse id="navbarScroll" className="d-flex justify-content-between align-items-center">
                        <Nav className="me-auto my-2 my-lg-0">
                            <Nav.Link as={NavLink} to="/">
                                Home
                            </Nav.Link>
                            <Nav.Link as={NavLink} to="/products/">
                                Products
                            </Nav.Link>
                            <NavDropdown title="Interesting Stuff" id="navbarScrollingDropdown">
                                <NavDropdown.Item as={Link}>About us</NavDropdown.Item>
                                <NavDropdown.Item as={Link}>Become a Seller</NavDropdown.Item>
                                <NavDropdown.Divider />
                                <NavDropdown.Item as={Link}>Contact Us</NavDropdown.Item>
                            </NavDropdown>
                        </Nav>
                        <Nav>
                            {(user.id !== null) && (
                                <>
                                    <Nav.Link as={NavLink} to="/cart">
                                        <FaShoppingCart />
                                    </Nav.Link>
                                    <NavDropdown title={<FaUser />} id="navbarScrollingDropdown" drop='down'>
                                        <NavDropdown.Item as={NavLink} to="/profile">
                                            Profile
                                        </NavDropdown.Item>
                                        <NavDropdown.Item as={NavLink} to="/userOrders">
                                            Orders
                                        </NavDropdown.Item>
                                        <NavDropdown.Divider />
                                        <NavDropdown.Item as={NavLink} to="/logout">
                                            Logout
                                        </NavDropdown.Item>
                                    </NavDropdown>
                                </>
                            )}
                            {(user.id === null) && (
                                <>
                                    <Nav.Link as={NavLink} to="/cart">
                                        <FaShoppingCart />
                                    </Nav.Link>
                                    <NavDropdown title={<FaUser />} id="navbarScrollingDropdown" drop='down'>
                                        <NavDropdown.Item as={NavLink} to="/login">
                                            Login
                                        </NavDropdown.Item>
                                        <NavDropdown.Item as={NavLink} to="/register">
                                            Register
                                        </NavDropdown.Item>
                                    </NavDropdown>
                                </>
                            )}
                            <Form className="d-flex align-items-center">
                                <Form.Control
                                    type="text"
                                    placeholder="Search"
                                    className="me-2"
                                />
                                <Button variant="outline-light">
                                    <FaSearch />
                                </Button>
                            </Form>
                        </Nav>
                    </Navbar.Collapse>
                </Container>
            </Navbar>
        </>
    );
}

export default AppNavbar;
