import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col, CardImg, Stack } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'
import { ListGroup } from 'react-bootstrap';

function formatCurrency(amount) {
    return new Intl.NumberFormat('en-PH', { style: 'currency', currency: 'PHP' }).format(amount);
}

export default function ProductView() {

    // Gets the productId from the URL of the route that this component is connected to. 
    const { productId } = useParams()
    const { user } = useContext(UserContext)
    const navigate = useNavigate()
    const [name, setName] = useState("");
    const [description, setDescription] = useState("");
    const [price, setPrice] = useState(0);
    const [image, setImage] = useState();
    const [qty, setQty] = useState(1)
    const [stocks, setStocks] = useState(0)

    const addQty = () => {
        if (qty >= 0)
            setQty(qty + 1)

    }

    const subQty = () => {
        if (qty > 0)
            setQty(qty - 1)

    }

    console.log(user)
    const buyNow = () => {
        if (user.isAdmin === true) {
            Swal.fire({
                title: "Unauthorize",
                icon: "warning",
                text: "Admins cannot purchase products."
            });
        } else {
            navigate(`/checkout/${productId}`);
        }
    };

    const addToCart = (productId) => {
        if (user.isAdmin === true) {
            Swal.fire({
                title: "Unauthorize",
                icon: "warning",
                text: "Admins cannot purchase products."
            })
        } else {
            fetch(`${process.env.REACT_APP_API_URL}/carts/addToCart`, {
                method: 'POST',
                headers: {
                    "Content-Type": "application/json",
                    Authorization: `Bearer ${localStorage.getItem('token')
                        } `
                },
                body: JSON.stringify({
                    products:
                        { "productId": productId, quantity: qty }
                })
            })
                .then(response => response.json())
                .then(result => {
                    if (result) {
                        Swal.fire({
                            title: "Success!",
                            icon: "success",
                            text: "Product added to cart!"
                        })

                        navigate('/products')
                    } else {
                        console.log(result)

                        Swal.fire({
                            title: "Something went wrong!",
                            icon: "error",
                            text: "Please try again"
                        })
                    }
                })
        }
    }

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/products/${productId} `)
            .then(response => response.json())
            .then(result => {
                console.log(result.name)
                console.log(result.price)
                console.log(result.image)
                setName(result.name)
                setDescription(result.description)
                setPrice(result.price)
                setStocks(result.stocks)
                setImage(result.image)
            })
    }, [productId])

    return (
        <><Container className="mt-5 text-center">
            <Row>
                <Col lg={{ span: 6, offset: 3 }}>
                    <Card>
                        <Card.Img variant="top" src={image} />
                        <Card.Body>
                            <Card.Title>{name}</Card.Title>
                            <Card.Text>
                                <Card.Subtitle>Description:</Card.Subtitle>
                                <Card.Text></Card.Text>
                                <Card.Subtitle>Price:</Card.Subtitle>
                                <Card.Text></Card.Text>
                            </Card.Text>
                            <ListGroup className="list-group-flush">
                                <ListGroup.Item>{description}</ListGroup.Item>
                                <ListGroup.Item>{formatCurrency(price)}</ListGroup.Item>
                                {(stocks !== 0) ?
                                    <ListGroup.Item>Stocks Left: {stocks}</ListGroup.Item>
                                    :
                                    <ListGroup.Item>Not Available</ListGroup.Item>
                                }
                                {(user.id !== null) &&
                                    <ListGroup.Item>
                                        <Button className="w-15 px-3 " variant="primary" onClick={subQty}>
                                            -
                                        </Button>
                                        <span className="mx-3">{qty}</span>
                                        <Button className="w-15 px-3 " variant="primary" onClick={addQty}>
                                            +
                                        </Button>
                                    </ListGroup.Item>
                                }
                            </ListGroup>

                            {user.id !== null ? (
                                <Stack direction="horizontal" className="justify-content-center">
                                    {qty !== 0 ? (
                                        <div className='w-25'>
                                            <Button className=" btn-block mr-2" variant="primary" onClick={() => buyNow(productId)}>
                                                Buy Now
                                            </Button>
                                        </div>
                                    ) : (
                                        <div className='w-25'>
                                            <Button className="btn-block mr-2" variant="primary" onClick={() => buyNow(productId)} disabled>
                                                Buy Now
                                            </Button>
                                        </div>
                                    )}

                                    {qty !== 0 ? (
                                        <div className='w-25'>
                                            <Button className="btn-block" variant="primary" onClick={() => addToCart(productId)}>
                                                Add to Cart
                                            </Button>
                                        </div>
                                    ) : (
                                        <div className='w-25'>
                                            <Button className="btn-block" variant="primary" onClick={() => addToCart(productId)} disabled>
                                                Add to Cart
                                            </Button>
                                        </div>
                                    )}
                                </Stack>
                            ) : (
                                <Link className="btn btn-success btn-block" to="/login">
                                    Login to proceed
                                </Link>
                            )}
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </Container>

        </>
    );
};
