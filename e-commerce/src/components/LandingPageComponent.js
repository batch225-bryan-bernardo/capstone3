import { Link } from "react-router-dom";
import UserContext from "../UserContext";
import { useContext } from "react";

function LandingPageComponent() {

    const { user } = useContext(UserContext)

    return (

        <div className="homepage-container">
            <div className="homepage-content">
                <div class="home-page-message">
                    <h1>EXCEED YOUR LIMITS!</h1>
                    <p>Start your journey with us.</p>
                    <p>Here at The Polar Health & Fitness Co.</p>
                </div>
                <div className="homepage-buttons">
                    {(user.id === null) ?
                        <>
                            <Link to="/register"><button>Register</button> </Link>
                            <Link to="/login"><button>Login</button> </Link>
                        </>
                        :
                        <Link to="/products"><button>Shop now!</button> </Link>
                    }
                </div>

            </div>
        </div>

    );
}

export default LandingPageComponent;
