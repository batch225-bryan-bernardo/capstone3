import { useState } from 'react';
import { Button, Form, Modal } from 'react-bootstrap';
import Swal from 'sweetalert2';

function RemoveProductModal({ product, productId, handleEditProduct, handleCloseModal }) {

    const [quantity, setQuantity] = useState(product.quantity);

    const handleEdit = () => {
        if (quantity <= 0) {
            Swal.fire({
                icon: 'error',
                title: 'Invalid quantity',
                text: 'The quantity must be greater than 0.',
            });
            return;
        }

        const editedProduct = {
            newQuantity: quantity,
        };
        console.log(product.productId);
        const token = localStorage.getItem('token');

        fetch(`${process.env.REACT_APP_API_URL}/carts/removeFromCart`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${token}`,
            },
            body: JSON.stringify({
                "products": { "productId": productId }
            })
        })
            .then((response) => response.json())
            .then((result) => {
                console.log(result);
                handleEditProduct(productId);
                handleCloseModal();
                Swal.fire({
                    icon: 'success',
                    title: 'Product Updated',
                    text: 'The product has been removed from cart.',
                });
            })
            .catch((error) => {
                console.error(error);
                Swal.fire({
                    icon: 'error',
                    title: 'Error',
                    text: 'An error occurred while removing the product.',
                });
            })
    };

    return (
        <Modal show={true} onHide={handleCloseModal}>
            <Modal.Header closeButton>
                <Modal.Title>Remove Product from Cart?</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Form>
                    <Form.Group>
                        <Form.Label>Name: {product.name}</Form.Label>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Price: {product.price}</Form.Label>
                    </Form.Group>
                </Form>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={handleCloseModal}>
                    Cancel
                </Button>
                <Button variant="danger" onClick={handleEdit}>
                    Remove
                </Button>
            </Modal.Footer>
        </Modal>
    );
}

export default RemoveProductModal;
